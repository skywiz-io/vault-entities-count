
import requests
import csv
from csv import writer
import os

if os.environ.get('VAULT_ADDR') and os.environ.get('VAULT_TOKEN') is not None:
    vault_addr = os.environ.get('VAULT_ADDR')
    vault_token = os.environ.get('VAULT_TOKEN')

    # Get Vault Namespaces
    vault_namespaces = {"root"}
    namespace = {
        'X-Vault-Token': vault_token,
    }

    namespace_response = requests.request('LIST', vault_addr + '/v1/sys/namespaces', headers=namespace)
    parsed_namespace = namespace_response.json()
    vault_namespaces_list=list(vault_namespaces)
    parsed_namespace_list=list(parsed_namespace)
    if parsed_namespace_list[0] != 'errors':
        for namespace in parsed_namespace["data"]["keys"]:
            vault_namespaces_list.append(namespace)
        num_of_namespaces = len(vault_namespaces_list)
    else: 
        num_of_namespaces = 1
    print('Number of Vault Namespaces: '+ ' ' + str(num_of_namespaces))
    # Loop Through Namespaces
    n = 0
    while(n < num_of_namespaces):
        vault_namespace = vault_namespaces_list[n]
        n += 1
        print("Working on Vault Namespace: "+ " " + vault_namespace)
        # Get Kube Hosts
        auth_methods_list = {
            'X-Vault-Token': vault_token,
            'X-Vault-Namespace': vault_namespace
        }
        response = requests.request('GET', vault_addr + '/v1/sys/auth', headers=auth_methods_list)
        parsed_auth_methods = response.json()["data"]
        kube_host = {}
        mount_types = {}
        
        for key, value in parsed_auth_methods.items():
            if value["type"] == 'kubernetes':
                config = {
                    'X-Vault-Token': vault_token,
                    'X-Vault-Namespace': vault_namespace
                }
                path = key + 'config'
                config = requests.request('GET', vault_addr + '/v1/auth/'+ path, headers=config)
                parsed_config = config.json()
                kube_host['auth/'+key] = parsed_config["data"]["kubernetes_host"]
            else:
                kube_host['auth/'+key] = ''
            mount_types[value["type"]] = 0
            
        ## Fetch the keys
        ## Key Will Increment By 1 each time using the count of the JSON array.
        fetch = {
            'X-Vault-Token': vault_token,
            'X-Vault-Namespace': vault_namespace
        }
        fetch_response = requests.request('LIST', vault_addr + '/v1/identity/entity-alias/id', headers=fetch)
        parsed_fetch = fetch_response.json()
        fetch_list = list(parsed_fetch)
        if fetch_list[0] != 'errors':
            num_of_keys = len(parsed_fetch["data"]["keys"])
            print('Number of Keys In Namespace' + ' ' + vault_namespace + ' = ' + str(num_of_keys))
            i = 0
            # fetch the keys
            while(i < num_of_keys and num_of_keys > 0):
                keys          = parsed_fetch["data"]["keys"][i]
                i            += 1
                if parsed_fetch["data"]["key_info"][keys]["mount_type"] != 'None' and parsed_fetch["data"]["key_info"][keys]["mount_type"] != "":
                    mount_type   = parsed_fetch["data"]["key_info"][keys]["mount_type"]
                    mount_types[mount_type] = mount_types[mount_type] + 1
                else:
                    mount_type = "None"
                header = {
                    'X-Vault-Token': vault_token,
                    'X-Vault-Namespace': vault_namespace
                }
                path = vault_addr + '/v1/identity/entity-alias/id/'+keys
                response = requests.get(path, headers=header)
                parsed_response = response.json()
                responses_values = parsed_response["data"]
                fieldnames = list(responses_values.keys())
                fieldnames.append("kube_host") 
                with open(mount_type+'.csv', 'a') as csvfile:
                    output = csv.DictWriter(csvfile, fieldnames = fieldnames)
                    if mount_types[mount_type] <= 1:
                        output.writeheader()
                    responses_values["kube_host"] = kube_host[responses_values["mount_path"]]
                    output.writerow(responses_values)
                    csvfile.close()
                    if i % 100 == 0:
                        print(str(i) + ' ' + "Out of " +  str(num_of_keys) + ' keys proccessed')
                    
            print('Number of Keys In Namespace' + ' ' + vault_namespace + ' = 0 ')

    print('Done')

else:
    print("You didn't set VAULT_ADDR and VAULT_TOKEN environment variables, please set them and re run the script!")


# Add loop to count the rows written to the csvs and notify every 20seconds








