- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Work Flow](#work-flow)
- [Values](#values)

## Introduction
Count Entites in your HashiCorp Vault environments.  
  
## Prerequisites
Docker  
  
## Work Flow
Build the Docker image:  
cd app  
docker build -t counter:v1  
  
Run a Docker Container:  
docker run -it --name test counter:v1  
  
Once the container is up and running the the scripts requires:  
Vault address, and Vault Token set as Environment Variables.  
In order to set those env variables:  
export VAULT_ADDR="YOUR_VAULT_URL"  
export VAULT_TOKEN="YOUR_VAULT_TOKEN"  
  
If Your using Vault Locally: Use this address when the script asks for your Vault URL.  
http://host.docker.internal:8200  
  
## Values
The script will output the values into different .csv files filtered by the Vault Auth Method it's attached to.  
In order to copy the .csv files to your local host use the following command:  
docker cp CONTAINER_NAME:/app/ ~/Desktop/Values     


