"use strict";

const { RateLimiter } = require('limiter'); // https://github.com/jhurliman/node-rate-limiter
const createCsvWriter = require('csv-writer').createArrayCsvWriter; // createObjectCsvWriter createArrayCsvWriter
const AsyncLock = require('async-lock');
const vaultApi = require('./vault-api.js');
const fs = require('fs/promises');
const fsSync = require('fs');
const Papa = require('papaparse');

/*** CONSTANTS ***/
const API_PER_SECOND = process.env.API_PER_SECOND || 10 // Take api limiter value from env. (Default is 10)
const VAULT_MAX_ASYNC_CALLS = process.env.VAULT_MAX_ASYNC_CALLS || "false" // Maximum number of async functions to queue before parallel execution

const limiter = new RateLimiter({ tokensPerInterval: API_PER_SECOND, interval: 'second'});
const lock = new AsyncLock();
const outputDirectory = "output/"
const checkpointFilename = `${outputDirectory}checkpoint.lock`
const errorFilename = `${outputDirectory}index-debug.log`
const namespaceDirectory = `${outputDirectory}namespace/`

/*** INIT ***/
let csvWriterMap = {}
let errorStream = fsSync.createWriteStream(errorFilename, { flags: 'a' });

/*** MAIN ***/
if (!fsSync.existsSync(outputDirectory)) {
  fsSync.mkdirSync(outputDirectory, { 'recursive': true });
}
async function main() {
  // List all namespaces
  let namespaces = await vaultApi.listNamespaces()
  namespaces.push("root/")
  // Checkpoint check
  let recoveredCheckpoint = false
  await readCheckpoint(checkpointFilename)
    .then(res => {
      if (res != 'init') {
        recoveredCheckpoint = namespaces.findIndex(element => element == res)
        console.log(`Recovered checkpoint.`)
        if (recoveredCheckpoint == -1) {
          throw Error(`Checkpoint file found but could not identify current namespace: ${res}`)
        }
        // Map of finished entities in latest namespace
        return listFinishedEntities(res)
      }
    })
    .then(res => {
      let mapFinishedEntities = res || {} // Map of finished entities in latest namespace
      // Loop through namespaces
      let namespaceLoop = async () => {
        for (const index in namespaces) {
          let namespace = namespaces[index]
          console.log(`Namespace: ${namespace}`)
          if (recoveredCheckpoint !== false && index < recoveredCheckpoint) {
            // Skip current namespace if not the recovered checkpoint
            console.log(`Recovered checkpoint. Skipping ${namespace}.`)
            continue;
          }
          // Update checkpoint to current namespace
          updateCheckpoint(checkpointFilename, namespace)

          // Create ns directories
          if (!fsSync.existsSync(`${namespaceDirectory}${namespace}`)) {
            fsSync.mkdirSync(`${namespaceDirectory}${namespace}`, {'recursive': true});
          }

          // List all auth
          await vaultApi.listAuthMethod(namespace)
            .then(res => {
              // Get unique list of auth types
              let authMethodList = Array.from(
                Object.values(res).reduce(
                  (previousValue, currentValue) =>
                    previousValue.add(currentValue.type),
                  new Set()
                )
              )
              // Create csv write streams for mounts
              authMethodList.forEach(authMethod => {
                let csvFileName = `${namespaceDirectory}${namespace}${authMethod}`
                csvWriterMap[csvFileName] = newCsv(csvFileName)
              });
            })
          // List entities
          let entityList = await vaultApi.listEntities(namespace)
          let currentIndex = 0
          let nextBreakpointIndex = entityList.length
          let allPromises = [];
          let maxAsyncCalls = Number(VAULT_MAX_ASYNC_CALLS)
          if (maxAsyncCalls) {
            nextBreakpointIndex = maxAsyncCalls
          }
          // Spawn async api call per entity
          while (currentIndex < entityList.length) {
            // Batch the queued async functions for parallel execution into smaller chunks
            for (; currentIndex < nextBreakpointIndex && currentIndex < entityList.length ; currentIndex++) {
              // console.log(process.memoryUsage());
              if (mapFinishedEntities[entityList[currentIndex]]) {
                writeError(errorStream, `Skipping already processed ${entityList[currentIndex]} in namespace ${namespace}`)
                return
              }
              allPromises.push(
                pipeline(namespace, entityList[currentIndex])
              );
            }
            await Promise.allSettled(allPromises)
            .then((results) => {
              results.forEach((result) => {
                if (result.status !== 'fulfilled') {
                  writeError(errorStream, result);
                }
              })
            });
            if (maxAsyncCalls) {
              nextBreakpointIndex += maxAsyncCalls
            }
          }
        }
      }
      namespaceLoop()
    })
}
main()

/*** FUNCTIONS ***/
async function listFinishedEntities(namespace) {
  let fileList = fsSync.readdirSync(`${namespaceDirectory}${namespace}`);
  let mapFinishedEntities = {}

  let promises = fileList.map(async fileName => {
    let file = fsSync.createReadStream(`${namespaceDirectory}${namespace}${fileName}`);

    Papa.parsePromise = function (file) {
      return new Promise(function (complete, error) {
        Papa.parse(file, {
          complete,
          error,
          header: false,
          delimiter: ',',
          linebreak: '\n',
          fastMode: true,
        });
      });
    };

    await Papa.parsePromise(file)
      .then(function (results, file) {
        // Create map with key as id for quick referencing
        results.data.reduce(function (map, row) {
          map[row[3]] = true; // Column of entity id used in api call
          return map;
        }, mapFinishedEntities)
      })

  })
  await Promise.all(promises)
  return mapFinishedEntities
}

async function readCheckpoint(checkpointFilename) {
  try {
    await fs.access(checkpointFilename, fs.F_OK)
    //file exists
    return fs.readFile(checkpointFilename, 'utf8');
  } catch (err) {
    console.log('Checkpoint does not exist. Assuming first run.');
    return 'init'
  }
}

function updateCheckpoint(checkpointFilename, content) {
  try {
    fsSync.writeFileSync(checkpointFilename, content, { flag: 'w+' });
  } catch (err) {
    writeError(errorStream, err);
  }
}

async function pipeline(namespace, entity) {
  await limiter.removeTokens(1);
  // get entity data
  let merged = await vaultGetEntity(namespace, entity)
    .catch(error => {
      return Promise.reject(`[ERR] The following entity: ${entity}, namespace: ${namespace} \n...failed with error: ${stringifyContent(error)}`)
  })
  // append to correct mount csv
  let csvFileName = `${namespaceDirectory}${namespace}${merged.mount_type}`
    // parse into csv format
    await writeToCsv(csvFileName, [Object.values(merged)])
      .catch(error => {
        writeError(errorStream, `[ERR] The following entity: ${entity}, namespace: ${namespace}, response: ${stringifyContent(merged)} \n...failed with error: ${stringifyContent(error)}`)
        return Promise.reject(`[ERR] The following entity: ${entity}, namespace: ${namespace} \n...failed with error: ${stringifyContent(error)}`)
      })
  return entity
}

async function vaultGetEntity(namespace, entity) {
  // get entity data
  let res = await vaultApi.getEntity(namespace, entity);
  let records = res;
  let metadata = {}
  try {
    metadata = res.metadata
    delete records.metadata
    let kube_host = {}
    if (res.mount_type == "kubernetes") {
      let response = await vaultApi.getAuthMethod(namespace, res.mount_path)
      kube_host['kubernetes_host'] = response.kubernetes_host
    }
    let merged = { ...records, ...metadata, ...kube_host };
    return merged
  } catch (error) {
    writeError(errorStream, `[ERR] failed with: ${stringifyContent(error)}. For namespace: ${namespace}, entity: ${entity}, and response: ${stringifyContent(res)}`);
    return Promise.reject(entity)
  }
}

async function writeToCsv(csvFileName, records) {
  return lock.acquire('key', async function () {
    await getCsv(csvFileName).writeRecords(records)
  })
}

/*** HELPERS ***/
function getCsv(mount) {
  if (csvWriterMap.hasOwnProperty(mount)) {
    return csvWriterMap[mount]
  }
  else {
    throw Error(`CSV file for mount type ${mount} not initialized. Check code.`)
  }
}

function newCsv(mount) {
  return createCsvWriter({
    append: true,
    path: `${mount}.csv`
  });
}

function writeError(writeStream, content) {
  try {
    let parsedContent = stringifyContent(content)
    errorStream.write(parsedContent + "\n");
  } catch (err) {
    console.error(err);
  }
}

function stringifyContent(content) {
  let stringifyContent = content.toString()
  if (stringifyContent == "[object Object]") {
    stringifyContent = JSON.stringify(content)
  }
  return stringifyContent
}
