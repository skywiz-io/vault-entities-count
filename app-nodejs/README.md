# Vault accessor count

- [Vault accessor count](#vault-accessor-count)
  - [Introduction](#introduction)
  - [Prerequisites](#prerequisites)
  - [Work Flow](#work-flow)
  - [Values](#values)
  - [WIP: Docker instructions](#wip-docker-instructions)

## Introduction

Count Entites in your HashiCorp Vault environments.  
  
## Prerequisites

[Node 18](https://nodejs.org/en/download/current/)

## Work Flow

```sh
export VAULT_ADDR="YOUR_VAULT_URL"  
export VAULT_TOKEN="YOUR_VAULT_TOKEN"  
export API_PER_SECOND="number of api calls to limit to the vault"
export VAULT_SCRIPT_MEM_GB="number of GB to allocate the running of this script"
export VAULT_SKIP_INSECURE="Ignore self signed certs"
export VAULT_MAX_ASYNC_CALLS="Maximum number of async functions to queue before parallel execution"
```

>NOTE: `API_PER_SECOND` is optional. 10 is the default.
>NOTE: `VAULT_SCRIPT_MEM_GB` is optional. 4 is the default.
>NOTE: `VAULT_SKIP_INSECURE` is optional. false is the default. (Set to "true" if needed).
>NOTE: `VAULT_MAX_ASYNC_CALLS` is optional. false is the default, as in no limit (Set to a number if needed).

Run the commands:

```sh
npm install
npm start
```

## Values

The script will output the values into different .csv files filtered by the Vault Auth Method it's attached to and nested in namespaced folders.

## WIP: Docker instructions

Build the Docker image:  
cd app-nodejs
docker build -t counter:v1  

Once the container is built, set the following:  
Vault address, and Vault Token set as Environment Variables.  
In order to set those env variables:  
export VAULT_ADDR="YOUR_VAULT_URL"  
export VAULT_TOKEN="YOUR_VAULT_TOKEN"  
export API_PER_SECOND="number of your choice"  
  
If Your using Vault Locally: Use this address when the script asks for your Vault URL.  
<http://host.docker.internal:8200>  
  
Run a Docker Container:  
docker run -it --name test counter:v1
docker run -e VAULT_ADDR -e VAULT_TOKEN -e API_PER_SECOND test counter:v1

In order to copy the .csv files to your local host use the following command:  
docker cp CONTAINER_NAME:/app/ ~/Desktop/Values
