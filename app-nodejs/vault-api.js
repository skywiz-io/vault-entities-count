"use strict";
const https = require('https');
const axios = require('axios');
// const { promisify } = require('util');
const fsSync = require('fs');

const VAULT_ADDR = process.env.VAULT_ADDR
const VAULT_TOKEN = process.env.VAULT_TOKEN
const VAULT_SKIP_INSECURE = process.env.VAULT_SKIP_INSECURE || "false" // Ignore self signed certs
const outputDirectory = "output/"
const errorFilename = `${outputDirectory}vaultapi-debug.log`

let errorStream = fsSync.createWriteStream(errorFilename, { flags: 'a' });

// Get Vault Namespaces
const headers = {
  'X-Vault-Token': VAULT_TOKEN,
}

const apiCall = axios.create({
  baseURL: VAULT_ADDR,
  timeout: 0, // default is `0` (no timeout)
  headers: headers
});

if (VAULT_SKIP_INSECURE === "true") {
  apiCall.defaults.httpsAgent = new https.Agent({
    rejectUnauthorized: false
  }) 
}

/*** MAIN ***/

async function listNamespaces() {
  try {
    let response = await apiCall({
      method: 'LIST',
      url: '/v1/sys/namespaces'
    })
    return response.data.data.keys
  } catch (error) {
    writeError(errorStream, stringifyContent(error))
    return []
  }
}

async function listAuthMethod(namespace) {
  try {
    let response = await apiCall({
      method: 'GET',
      url: '/v1/sys/auth',
      headers: {
        'X-Vault-Namespace': namespace
      }
    })
    return response.data.data
  } catch (error) {
    writeError(errorStream, stringifyContent(error))
  }
}

// Primarily for getting kube_host in k8s method
async function getAuthMethod(namespace, authPath) {
  try {
    let response = await apiCall({
      method: 'GET',
      url: `/v1/${authPath}config`,
      headers: {
        'X-Vault-Namespace': namespace
      }
    })
    return response.data.data
  } catch (error) {
    writeError(errorStream, stringifyContent(error))
  }
}

async function listEntities(namespace) {
  try {
    let response = await apiCall({
      method: 'LIST',
      url: '/v1/identity/entity-alias/id',
      headers: {
        'X-Vault-Namespace': namespace
      }
    })
    return response.data.data.keys
  } catch (error) {
    writeError(errorStream, stringifyContent(error))
    return []
  }
}

async function getEntity(namespace, entity) {
  try {
    let response = await apiCall({
      method: 'GET',
      url: `/v1/identity/entity-alias/id/${entity}`,
      headers: {
        'X-Vault-Namespace': namespace
      }
    })
    return response.data.data
  } catch (error) {
    writeError(errorStream, stringifyContent(error))
  }
}

async function listAccessors(namespace) {
  try {
    let response = await apiCall({
      method: 'LIST',
      url: `/v1/auth/token/accessors`,
      headers: {
        'X-Vault-Namespace': namespace
      }
    })
    return response.data.data.keys
  } catch (error) {
    writeError(errorStream, stringifyContent(error))
    return []
  }
}

async function getAccessor(namespace, accessor) {
  try {
    let response = await apiCall({
      method: 'POST',
      url: `/v1/auth/token/lookup-accessor`,
      headers: {
        'X-Vault-Namespace': namespace
      },
      data: {
        'accessor': accessor
      }
    })
    return response.data.data
  } catch (error) {
    writeError(errorStream, stringifyContent(error))
  }
}

// const ExternalAPIcall = (params, callback) => {
//   setTimeout(() => {
//     callback(null, { status_message: params.number });
//   }, 0);
// };
// const dummyApi = promisify(ExternalAPIcall);

module.exports = {
  listNamespaces,
  listAuthMethod,
  getAuthMethod,
  listEntities,
  getEntity,
  listAccessors,
  getAccessor
  // dummyApi
}

/*** HELPERS ***/
function writeError(writeStream, content) {
  try {
    let parsedContent = stringifyContent(content)
    errorStream.write(parsedContent + "\n");
  } catch (err) {
    console.error(err);
  }
}

function stringifyContent(content) {
  let stringifyContent = ""
  if (typeof (content) == "object") {
    stringifyContent = JSON.stringify(content)
  } else {
    stringifyContent = content.toString()
  }
  return stringifyContent
}